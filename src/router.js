import Vue from 'vue'
	import Router from 'vue-router'
	 
    import MyCal from './views/MyCal.vue'
    import MyPin from './views/StickBoard.vue'
	import home from './views/home.vue'
	import MyToDo from './views/ToDo.vue'

	Vue.use(Router)
	export default new Router({
	  routes: [
		{
		  path: '/calendar',
		  name: 'Calendar',
		  component: MyCal
		},
		{
		  path: '/pinboard',
		  name: 'Pinboard',
		  component: MyPin
		},
        {
            path: '/ ',
            name: 'home',
            component: home
		},
		{
			path: '/ToDo',
			name: 'todo',
			component: MyToDo
		  },
	  ]
	})